#!/bin/bash

: ${DB_SECRET:=db_pass}
: ${MM_HOME:=/opt/mattermost}

: ${DB_HOST:=db}
: ${DB_PORT_NUMBER:=5432}
: ${MM_USERNAME:=mmuser}
: ${MM_PASSWORD:=mmuser_password}
: ${MM_DBNAME:=mattermost}
: ${MM_CONFIG_DIR:=${MM_HOME}/config}
: ${MM_CONFIG:=${MM_CONFIG_DIR}/config.json}
: ${MM_DB_PARAMS:="sslmode=disable&connect_timeout=10"}

generate_salt() {
  # this generates a nice random string for encryption and other goodies
  cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 48 | head -n 1
}

edit_config() {
  if [[ $2 = 'false' || $2 = 'true' || $2 =~ ^[[:digit:]]+$ ]]; then
    # don't quote non-strings
    jq "$1 = $2" $MM_CONFIG > $MM_CONFIG.tmp && mv $MM_CONFIG.tmp $MM_CONFIG
  else
    jq "$1 = \"$2\"" $MM_CONFIG > $MM_CONFIG.tmp && mv $MM_CONFIG.tmp $MM_CONFIG
  fi
}

if [ "${1:0:1}" = '-' ]; then
  set -- mattermost "$@"
fi

if [ "$1" = 'mattermost' ]; then
  for ARG in $@;
  do
    case "$ARG" in
      -config=*)
        MM_CONFIG=${ARG#*=};;
    esac
  done

  if [ ! -f $MM_CONFIG ]; then
    echo "No configuration file" $MM_CONFIG
    echo "Creating a new one"
    # Copy default configuration file
    cp ${MM_HOME}/config.json.orig $MM_CONFIG

    # Substitue some parameters with jq
    edit_config .ServiceSettings.ListenAddress ":80"
    edit_config .LogSettings.EnableConsole false
    edit_config .LogSettings.ConsoleLevel INFO
    edit_config .FileSettings.Directory ${MM_HOME}/data/
    edit_config .FileSettings.EnablePublicLink true
    edit_config .FileSettings.PublicLinkSalt $(generate_salt)
    edit_config .EmailSettings.SendEmailNotifications false
    edit_config .EmailSettings.FeedbackEmail ""
    edit_config .EmailSettings.SMTPServer ""
    edit_config .EmailSettings.SMTPPort ""
    edit_config .EmailSettings.InviteSalt $(generate_salt)
    edit_config .EmailSettings.PasswordResetSalt $(generate_salt)
    edit_config .RateLimitSettings.Enable true
    edit_config .SqlSettings.DriverName postgres
    edit_config .SqlSettings.AtRestEncryptKey $(generate_salt)
  else
    echo "Using existing config file" $MM_CONFIG
  fi

  # we want to overwrite the database settings in case they change from outside
  if [ -z "$MM_SQLSETTINGS_DATASOURCE"]; then
    echo -ne "Configure database connection..."
    export MM_SQLSETTINGS_DATASOURCE="postgres://$MM_USERNAME:$MM_PASSWORD@$DB_HOST:$DB_PORT_NUMBER/$MM_DBNAME?$MM_DB_PARAMS"
    edit_config .SqlSettings.DataSource $MM_SQLSETTINGS_DATASOURCE
    echo OK
  else
    echo "Using provided database connection"
  fi

  echo "Wait until database $DB_HOST:$DB_PORT_NUMBER is ready..."
  until nc -z $DB_HOST $DB_PORT_NUMBER; do
    sleep 1
  done

  # Wait to avoid "panic: Failed to open sql connection pq: the database system is starting up"
  sleep 1

  echo "Starting Mattermost."
fi

exec "$@"
