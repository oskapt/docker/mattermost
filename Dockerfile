FROM ubuntu:18.04

# Some ENV variables
ENV PATH="/opt/mattermost/bin:${PATH}"

# Install some needed packages
RUN apt-get -qq update && apt-get -qq -y install \
      curl jq netcat && \
    rm -rf /var/lib/apt/lists/*

# Prepare Mattermost
ENV MM_VERSION=5.35.1
RUN mkdir -p /opt/mattermost/data && \
    cd /opt && \
    curl -s https://releases.mattermost.com/$MM_VERSION/mattermost-$MM_VERSION-linux-amd64.tar.gz | tar -xzf - && \
    mv /opt/mattermost/config/config.json /opt/mattermost/config.json.orig

# Configure entrypoint
COPY docker-entrypoint.sh /
ENTRYPOINT ["/docker-entrypoint.sh"]

VOLUME /opt/mattermost/data

WORKDIR /opt/mattermost

CMD ["mattermost"]
