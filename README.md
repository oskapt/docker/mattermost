# Mattermost for Kubernetes

This is [Mattermost](https://www.mattermost.com), built for Kubernetes.

## Variables

- `DB_HOST` - your database host [db]
- `DB_PORT_NUMBER` - your database port number [5432]
- `MM_HOME` - the Mattermost root (/opt/mattermost)
- `MM_USERNAME` - the database user [mmuser]
- `MM_PASSWORD` - the database password [mmuser_password]
- `MM_DBNAME` - the database name [mattermost]
- `MM_CONFIG_DIR` - where to find the config ($MM_HOME/config)
- `MM_CONFIG` = name of the config file ($MM_CONFIG_DIR/config.json)
- `MM_DB_PARAMS` = extra database parameters (sslmode=disable&connect_timeout=10)

## Deployment

### Summary

- create a Secret for `MM_PASSWORD`
- create a PV
- create a Deployment
  - set `DB_HOST` and `MM_USERNAME` as environment variables
  - attach your Secret
  - set a health check on port 80 for `/system/ping`
  - create a PVC with the following sub-paths and mount points:
    - `config`: `/opt/mattermost/config`
    - `data`: `/opt/mattermost/data`
    - `logs`: `/opt/mattermost/logs`
    - `plugins`: `/opt/mattermost/plugins`
- create a Service and/or Ingress for port 80
- wrap it all in a certificate from LetsEncrypt using `cert-manager`
